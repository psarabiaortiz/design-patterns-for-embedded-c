/*
 * Copyright (c) 2021, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *  MotorProxy.c
 *
 *  Created on: 28 Apr. 2021
 *  Author: Pablo Sarabia Ortiz 		<psarabia@b105.upm.es>
 *  This code is a revision of the code provided in the book:
 * 
 *  Design patterns for embedded C: an embedded software engineering toolkit / 
 *  Bruce Powel Douglass – 1st ed.
 */

#include "MotorProxy.h"


/* class motorProxy */
/* This function takes a MotorData structure and creates */
/* a device-specific uint16_t in device native format. */

static uint16_t  marshal(motorProxy_t* const me, const struct motorData_t mData);
static struct motorData_t unmarshal(motorProxy_t* const me, uint16_t encodedMData);

void motorProxy_Init(motorProxy_t* const me) { 
	me->motorAddr = NULL;
	me->rotaryArmLength = 10; /*Default arm length*/
}

void motorProxy_Cleanup(motorProxy_t* const me) {
}

int16_t motorProxy_accessMotorDirection(motorProxy_t* const me, 
 direction_t* directionPtr) {
	motorData_t mData;
	int16_t status = -1; /*Error status*/
	if (!me->motorAddr) {return status;}
	mData = unmarshal(me, *me->motorAddr); 
	*directionPtr = mData.direction;
	status = 0; /*Non Error*/
	return status;
}

int16_t motorProxy_accessMotorSpeed(motorProxy_t* const me, uint16_t* speedPtr) {
	motorData_t mData;
	uint16_t motorSpeed; 
	double linearSpeed;
	int16_t status = -1; /*Error status*/
	if (!me->motorAddr) {return status;}
	mData = unmarshal(me, *me->motorAddr);
	motorSpeed = mData.speed;
	linearSpeed = round(motorSpeed*(2*PI*V_MAX*(me->rotaryArmLength)) / 
					(1000*(MAX_SPEED_INT -1)));
	/*Function to calculate the speed of the motor to get the desired linear 
	speed in the arm. 
	speed [m/s]= motorSpeed*(2*PI* VMax [rpm] * armLength [mm])/
					(1000 [mm/m] * MaxMotorSpeedBITs-1)) /
					 
	*/
	
	if (linearSpeed > UINT16_MAX){
		status = -2; /*There is overflow in the representation of linearSpeed*/
		*speedPtr = UINT16_MAX;
	}
	else{
		*speedPtr = (int16_t)linearSpeed;
		status = 0; /*Non Error*/
	}
	return status;
}

int16_t motorProxy_accessMotorState(motorProxy_t* const me, uint16_t* statePtr) {
    motorData_t mData;
	int16_t status = -1; /*Error status*/
	if (!me->motorAddr) {return status;}
	mData = unmarshal(me, *me->motorAddr);
	status = 0; /*Non Error*/
	return status;
}

int16_t motorProxy_clearErrorStatus(motorProxy_t* const me) {
	int16_t status = -1; /*Error status*/
	if (!me->motorAddr) {return status;}
	*me->motorAddr &= 0xFF;
	status = 0; /*Non Error*/
	return status;
}

void motorProxy_configure(motorProxy_t* const me, uint16_t length, 
 uint16_t* location) {
	if(length == 0){
		/*If the input length is zero the default size is 10 mm*/
		me->rotaryArmLength = 10;
	} 	
	else{
		me->rotaryArmLength = length;
	}
	
	me->motorAddr = location;	
}

int16_t motorProxy_disable(motorProxy_t* const me) {
// and with all bits set except for the enable bit
	int16_t status = -1; /*Error status*/
	if (!me->motorAddr) {return status;}
	*me->motorAddr &= 0xFFFE;
	status = 0; /*Non Error*/
	return status;
}

int16_t motorProxy_enable(motorProxy_t* const me) {
	int16_t status = -1; /*Error status*/
	if (!me->motorAddr) {return status;}
	*me->motorAddr |= 1 ;
	status = 0; /*Non Error*/
	return status;
}

int16_t motorProxy_initialize(motorProxy_t* const me) {
	motorData_t mData;
	int16_t status = -1; /*Error status*/
	if (!me->motorAddr) {return status;}
	mData.on_off = 1;
	mData.direction = 0;
	mData.speed = 0;
	mData.errorStatus = 0;
	mData.noPowerError = 0;
	mData.noTorqueError = 0;
	mData.BITError = 0;
	mData.overTemperatureError = 0;
	mData.reservedError1 = 0;
	mData.reservedError2 = 0;
	mData.unknownError = 0;
	*me->motorAddr = marshal(me, mData);
	status = 0; /*Non Error*/
	return status;
}

int16_t motorProxy_writeMotorSpeed(motorProxy_t* const me, 
 const direction_t direction, uint16_t speed) {
	motorData_t mData;
	double circularSpeed;
	uint16_t motorSpeed;
	int16_t status = -1; /*Error status*/
	if (!me->motorAddr) {return status;}
	mData = unmarshal(me, *me->motorAddr);
	mData.direction = direction;
	circularSpeed = round( 1000*(speed*(MAX_SPEED_INT -1)) / 
					(2*PI*V_MAX*(me->rotaryArmLength)));
	/*Function to calculate the speed of the motor to get the desired linear 
	speed in the arm. 
	motorSpeed[bit] = (1000 [mm/m] * speed [m/s] * MaxMotorSpeedBITs[rpm/bit]-1)) /
					2*PI* VMax [rpm] * armLength [mm])*/
	if (circularSpeed > (MAX_SPEED_INT -1) ){
		motorSpeed = MAX_SPEED_INT -1; /*Speed cannot be larger than 31*/
		status = -2; /*Speed overflow*/
	}
	else{
		motorSpeed = (uint16_t)circularSpeed;
		status = 0; /*Non Error*/;
	}
	mData.speed = motorSpeed;		
	*me->motorAddr = marshal(me, mData);	
	return status;
}
static uint16_t marshal(motorProxy_t* const me, motorData_t mData) {
	uint16_t deviceCmd;
	deviceCmd = 0; // set all bits to zero
	if (mData.on_off){ deviceCmd |= 1;} // OR in the appropriate bit
	if (mData.direction == FORWARD) {deviceCmd |= (2 << 1);}
	else if (mData.direction == REVERSE) {deviceCmd |= (1 << 1);}
	if (mData.speed < 32 && mData.speed >= 0){
		deviceCmd |= mData.speed << 3;
	}
	if (mData.errorStatus) {deviceCmd |= 1 << 8;}
	if (mData.noPowerError) {deviceCmd |= 1 << 9;}
	if (mData.noTorqueError) {deviceCmd |= 1 << 10;}
	if (mData.BITError) {deviceCmd |= 1 << 11;}
	if (mData.overTemperatureError) {deviceCmd |= 1 << 12;}
	if (mData.reservedError1) {deviceCmd |= 1 << 13;}
	if (mData.reservedError2) {deviceCmd |= 1 << 14;}
	if (mData.unknownError) {deviceCmd |= 1 << 15;}
	return deviceCmd;
}

static motorData_t unmarshal(motorProxy_t* const me, uint16_t encodedMData) {
	motorData_t mData;
	int temp;
	mData.on_off = encodedMData & 1;
	temp = (encodedMData >> 1) & 3; 	
	if (temp == 1){
		mData.direction = REVERSE;
	}
	else if (temp == 2){
		mData.direction = FORWARD;
	}
	else{
		mData.direction = NO_DIRECTION;
	}
	mData.speed = (encodedMData >> 3) & 31 ;
	mData.errorStatus = (encodedMData >> 8) & 1 ;
    mData.noPowerError = (encodedMData>> 9) & 1 ;
    mData.noTorqueError = (encodedMData >> 10) & 1 ; 
    mData.BITError = (encodedMData >> 11) & 1 ; 
    mData.overTemperatureError = (encodedMData >> 12) & 1 ; 
    mData.reservedError1 = (encodedMData >> 13) & 1 ; 
    mData.reservedError2 = (encodedMData >> 14) & 1;
	mData.unknownError = (encodedMData >> 15) & 1;
	return mData;
}
motorProxy_t * motorProxy_Create(void) {
	motorProxy_t* me = (motorProxy_t *) malloc(sizeof(motorProxy_t));
	if(me!=NULL){
		motorProxy_Init(me);
	}
	return me;
}
void motorProxy_Destroy(motorProxy_t* const me) {
	if(me!=NULL){
		motorProxy_Cleanup(me);
	}
	free(me);
}