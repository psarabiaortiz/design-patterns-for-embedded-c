/*
 * Copyright (c) 2021, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * MotorProxy.h
 *
 *  Created on: 28 Apr. 2021
 *  Author: Pablo Sarabia Ortiz 		<psarabia@b105.upm.es>
 *  This code is a revision of the code provided in the book:
 * 
 *  Design patterns for embedded C: an embedded software engineering toolkit / 
 *  Bruce Powel Douglass – 1st ed.
 */

#ifndef MotorProxy_H
#define MotorProxy_H

#include "HWProxyExample.h"
#include "MotorData.h"
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/*Constants*/
#define V_MAX 600 /*Max value in rpm of the motor, correspond to 11111 (31d)*/
#define MAX_SPEED_INT 32 /*Resolution in bits of the speed*/
#define PI 3.1415

/* class motorProxy */
typedef struct motorProxy_t motorProxy_t;
/* This is the proxy for the motor hardware. */
/* Speed is the linear speed at the arm of the motor in m/s*/
/* arm length in mm*/
/* If no arm length given then is 10 mm*/ 
struct motorProxy_t {
	uint16_t* motorAddr;
	uint16_t rotaryArmLength;	
};

/* Sets the initial conditions (pointer to null and arm value to 10), 
called by motorProxy_Create after malloc*/
void motorProxy_Init(motorProxy_t* const me);

/* Sets the exit conditions, called by motorProxy_Destroy before free*/
void motorProxy_Cleanup(motorProxy_t* const me);

/* Reads from the motor the direction of the motor and writes it in directionPtr
 returns 0 if the reading operation is correct or -1 if there was an error*/
int16_t motorProxy_accessMotorDirection(motorProxy_t* const me, 
 direction_t* directionPtr);

/* Reads from the motor the speed and writes the linear speed in the speedPtr (m/s)
returns 0 if the reading operation is correct or -1 if there was an undefined error or
-2 if there is overflow in writing the speedPtr */
int16_t motorProxy_accessMotorSpeed(motorProxy_t* const me, uint16_t* speedPtr);

int16_t motorProxy_accessMotorState(motorProxy_t* const me, uint16_t* statePtr);

/* keep all settings the same but clear error bits */ 
int16_t motorProxy_clearErrorStatus(motorProxy_t* const me);

/* Configure must be called first, since it sets up the */
/* address of the device. */
void motorProxy_configure(motorProxy_t* const me, uint16_t length, 
 uint16_t*location);

/* turn motor off but keep original settings */
int16_t motorProxy_disable(motorProxy_t* const me);

/* Start up the hardware but leave all other settings of the */
/* hardware alone */ 
int16_t motorProxy_enable(motorProxy_t* const me);

/* precondition: must be called AFTER configure() function. */ 
/* turn on the hardware to a known default state. */ 
int16_t motorProxy_initialize(motorProxy_t* const me);

/* Calculate the required circular speed to reach the specified linear speed in m/s
and writes it down to the motor register
Returns 0 if it is written down correctly -1 if there is an undefined error or 
-2 if there is overflow*/
int16_t motorProxy_writeMotorSpeed(motorProxy_t* const me, 
 const direction_t direction,uint16_t speed);

 /* Creates the object and allocs the space*/
motorProxy_t* motorProxy_Create(void); 

/* Deletes the object and free the space*/
void motorProxy_Destroy(motorProxy_t* const me);


#endif