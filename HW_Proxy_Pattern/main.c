/*
 * Copyright (c) 2021, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *  main.c
 *
 *  Created on: 28 Apr. 2021
 *  Author: Pablo Sarabia Ortiz 		<psarabia@b105.upm.es>
 *  This code is a revision of the code provided in the book:
 * 
 *  Design patterns for embedded C: an embedded software engineering toolkit / 
 *  Bruce Powel Douglass – 1st ed.
 */
#include "MotorProxy.h"

uint16_t currentSpeed = 123;
uint16_t* currentSpeedPtr = &currentSpeed;
uint16_t currentStatus = 10;
uint16_t desiredSpeed;
int16_t status;
direction_t direction = FORWARD;
direction_t actualDirection;
direction_t* actualDirectPtr = &actualDirection;
uint16_t length = 0;
uint16_t registerValue = 0; 
/*Mock the motor by allocating a uint16_t in memory*/
uint16_t* motorAddress = &registerValue; /*The direction of memory where is the motor*/
struct motorProxy_t* motorProxyID;
int main(){
   
    printf("The address of the motor is: %d, the value is: %d\n",
     motorAddress,*motorAddress);
    
    printf("Create a Motor Interface Example\n");
    

    motorProxyID = motorProxy_Create();
    motorProxy_configure(motorProxyID, length, motorAddress);
    motorProxy_initialize(motorProxyID);
    printf("The address of the motor is: %d, the value is: %d\n",
     motorAddress,*motorAddress);
    motorProxy_enable(motorProxyID);
    motorProxy_accessMotorDirection(motorProxyID, actualDirectPtr);
    printf("The direction of the motor is: %d\n",*actualDirectPtr);
    direction = REVERSE;
    
    motorProxy_writeMotorSpeed(motorProxyID, direction,0);
    
    motorProxy_accessMotorDirection(motorProxyID, actualDirectPtr);
    printf("The direction of the motor is: %d\n",*actualDirectPtr);
    printf("Access the speed of the Motor:\n");
    motorProxy_accessMotorSpeed(motorProxyID, currentSpeedPtr);
    printf("Speed: %d\n", *currentSpeedPtr);
    motorProxy_accessMotorDirection(motorProxyID, actualDirectPtr);
    printf("The direction of the motor is: %d\n",*actualDirectPtr);
    
    printf("Enter new speed: \n");
    scanf("%d", &desiredSpeed);

    motorProxy_writeMotorSpeed(motorProxyID,direction,desiredSpeed);
    printf("The address of the motor is: %d, the value is: %d\n",
     motorAddress,*motorAddress);
    motorProxy_accessMotorSpeed(motorProxyID, currentSpeedPtr);
    printf("Speed: %d\n", *currentSpeedPtr);
    motorProxy_Destroy(motorProxyID);

    return 0;
}