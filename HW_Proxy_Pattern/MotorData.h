/*
 * Copyright (c) 2021, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *  MotorData.h
 *
 *  Created on: 28 Apr. 2021
 *  Author: Pablo Sarabia Ortiz 		<psarabia@b105.upm.es>
 *  This code is a revision of the code provided in the book:
 * 
 *  Design patterns for embedded C: an embedded software engineering toolkit / 
 *  Bruce Powel Douglass – 1st ed.
 */
#ifndef MotorData_H
#define MotorData_H

#include "HWProxyExample.h"
typedef struct motorData_t motorData_t;
struct motorData_t {
    unsigned char on_off; /* Bit 0, 0 Off, 1 On*/
    direction_t direction; /*Bit 1-2, 00 Off, 01 Forward, 11 Backward  */
    uint16_t speed; /*Bit 3-7 speed up to 32 */
    bool errorStatus; /*Bit 8 Error, 1 any error */ 
    bool noPowerError; /*Bit 9 Power Error */
    bool noTorqueError; /*Bit 10 Torque Error */
    bool BITError; /*Bit 11 BITError */
    bool overTemperatureError; /* Bit 12 Temperature Error */
    bool reservedError1; /* Bit 13 Reserved Error 1 */  
    bool reservedError2; /*Bit 14 Reserved Error 2 */
    bool unknownError; /*Bit 15 Unknown Error */
};
#endif