# Design Patterns for Embedded C

## Introduction 
This a repository that shows a simple example of design patterns applied to embedded C.
The original idea was taken from the book Design Patterns for Embedded C. However, the examples had to be corrected it in order to compile.
This code should be an example of how to make a working pattern that improves the quality of the code.

## Available Examples
Currently only the hardware proxy pattern is available, any pull request with more code examples will be appreciated.
### Hardware Proxy Pattern
This pattern allows to create an abstract model of the hardware so the program can work at a higher level of abstraction and be compatible with any future changes of hardware.

## Available Slides
The slides are only available in Spanish as part of a course given in  the B105 Electronic Systems Lab at the UPM.

## References
_Bruce Powel Douglass. 2010. Design Patterns for Embedded Systems in C: An Embedded Software Engineering Toolkit (1st. ed.). Newnes, USA._

_Brian W. Kernighan and Dennis M. Ritchie. 1988. The  C Programming Language (2nd. ed.). Prentice Hall Professional Technical Reference._